package com.example.administrator.rxjava;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;


public class MainActivity extends AppCompatActivity {
    Subscription subscription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Observable observable = Observable.interval(1000, TimeUnit.MILLISECONDS).map(new Func1<Long, String>() {


            @Override
            public String call(Long aLong) {
                if (aLong >= 10) {
                    if (subscription != null) {
                        subscription.unsubscribe();
                    }
                }
                return aLong.toString();
            }
        });
        subscription = observable.subscribe(new Observer<String>() {
            @Override
            public void onCompleted() {
                Log.i("rx", "completed1");
            }

            @Override
            public void onError(Throwable e) {
                Log.i("rx", "" + e.getMessage());

            }

            @Override
            public void onNext(String s) {
                Log.i("rx", "time>>>" + new Date().toString() + ">>>" + s);
            }
        });
    }
}
